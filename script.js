let iconMenu = document.querySelector('.mobile-nav-control');
let menuItems = document.querySelectorAll('.main-nav li a');

let animationMenu = bodymovin.loadAnimation({
        container: iconMenu,
        renderer: 'svg',
        loop: false,
        autoplay: false,
        path: "assets/hamburger.json"
});

var directionMenu = 1;
const toggleMenu = () => {
  animationMenu.setDirection(directionMenu);
  animationMenu.play();
  directionMenu = -directionMenu;
}
iconMenu.addEventListener('click', (e) => {
  toggleMenu();
});

menuItems.forEach(menuItem => {
  menuItem.addEventListener('click', (e) => {
    toggleMenu();
  });
})



//accordion
const accordions = document.querySelectorAll('[data-acc-trigger]');

const closeOtherAcc = (notToClose) => {
  accordions.forEach(function(accordion, index){
    if (index != notToClose) {
      const accID = accordion.dataset.accTrigger;
      const accContentWrapper = document.querySelector(`[data-acc-content-wrapper="${accID}"]`);
      const accContent = accContentWrapper.querySelector('[data-acc-content]');
      const transitionDuration = accContentWrapper.dataset.accTransitionDuration;
      accContentWrapper.style.transitionDuration = transitionDuration / 1000 + "s"
      
      if (accContentWrapper.classList.contains('active')) {
        const contentHeight = accContent.offsetHeight;
        const transitionTime = accContentWrapper.style.transitionDuration;
        console.log(transitionTime);
        accContentWrapper.style.height = contentHeight + "px"
    
        accContentWrapper.classList.remove('active');
        accordion.classList.remove('active');
        
        setTimeout(function() {
          accContentWrapper.style.height = "0px";
        }, 100);
      };
    }
  });
}

accordions.forEach(function(accordion, index){
  const accID = accordion.dataset.accTrigger;
  const accContentWrapper = document.querySelector(`[data-acc-content-wrapper="${accID}"]`);
  const accContent = accContentWrapper.querySelector('[data-acc-content]');
  const transitionDuration = accContentWrapper.dataset.accTransitionDuration;
  let timeout
  
  accContentWrapper.style.transitionDuration = transitionDuration / 1000 + "s"
  
  accordion.addEventListener('click', function() {
    const contentHeight = accContent.offsetHeight;
    const transitionTime = accContentWrapper.style.transitionDuration;
    console.log(transitionTime);
    accContentWrapper.style.height = contentHeight + "px"
    clearTimeout(timeout);
    
    if (accContentWrapper.classList.contains('active')) {
      accContentWrapper.classList.remove('active')
      accordion.classList.remove('active')
      
      timeout = setTimeout(function(){
        accContentWrapper.style.height = "0px"
      }, 100)
    } else {
      accContentWrapper.classList.add('active')
      accordion.classList.add('active')
      
      timeout = setTimeout(function(){
        accContentWrapper.style.height = "auto"
      }, transitionDuration)

      closeOtherAcc(index);
    }
    
  })
});

const specialitiesSliderElm = document.querySelector('.twister-splide-slider-specialities')

//Booking slider
const specialitiesSlider = new Splide( specialitiesSliderElm, {
  type: 'loop',
  autoWidth: true,
  focus: 'center',
  pagination: false
} ).mount();

const specialitiesTextElm = document.querySelector('.specialities__slide-text-wrapper')

const initialTextElm = document.querySelector('.specialities__slide-text.active')
specialitiesTextElm.style.height = initialTextElm.offsetHeight + "px"

const initialSlideElm = document.querySelector('.specialities__slide:not(.splide__slide--clone)[data-specialities-slide-index="0"]')
initialSlideElm?.classList.add('pre-active')

specialitiesSlider.on( 'move', moveObj => {
  const currentTextElm = document.querySelector('.specialities__slide-text.active')
  const nextTextElm = document.querySelector(`[data-specialities-text-slide-index="${moveObj}"]`)
  const currentSlideElm = document.querySelector('.specialities__slide.pre-active')
  const nextSlideElm = document.querySelector(`.specialities__slide:not(.splide__slide--clone)[data-specialities-slide-index="${moveObj}"]`)
  
  currentTextElm?.classList.remove('active')
  nextTextElm?.classList.add('active')
  
  currentSlideElm?.classList.remove('pre-active')
  nextSlideElm?.classList.add('pre-active')

  specialitiesTextElm.style.height = nextTextElm.offsetHeight + "px"

  specialitiesSliderElm.classList.add('moving')
} );

specialitiesSlider.on( 'moved', function () {
  specialitiesSliderElm.classList.remove('moving')
} );

//Booking slider
new Splide( '.twister-splide-slider', {
  type   : 'loop',
  arrows: false,
} ).mount();